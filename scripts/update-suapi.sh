#!/bin/sh

# pull new version
cd /home/pi/suapi
git pull origin master

# restart suapi.service
sudo systemctl restart suapi.service