# aifoon - API - Swarm Unit

### Version

0.1

### General

API for a swarm unit. Change the config and README file when changes were made.

## Documentation API

### helloworld

This will result in a 'Hello World' response to see if the API works.

### updateapi

Do an API update via GIT and reload the suapi.service on the swarm unit.

### updatepd

Do an update of the Pure Data files via GIT and reboot the swarm unit so the new config will load again.

### updatestartup

Do an update of the startup scripts via GIT.

### Author

Written by Tim De Paepe (tim.depaepe@gmail.com)