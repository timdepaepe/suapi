/**
 * SWARM UNIT API - server
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

// set the base directory
global.__basedir    = __dirname;
global.__appdir     = __dirname + '/app';
global.__modeldir   = __dirname + '/app/model';

// create
const express       = require('express');
const api           = express();
const webserver     = express();
const bodyParser    = require('body-parser');
const config        = require('config');
const apiPort       = config.get('apiPort');
const webPort       = config.get('webPort');

// the logger
let Logger = require('./app/model/logger.js');

// ------------
// API
// ------------

// use bodyparser
api.use(bodyParser.urlencoded({extended: true}));

// require the api routes
require('./app/routes/api_routes')(api, {});

// listen to our apiPort
api.listen(apiPort, ()=> {
    Logger.log('info', 'Swarm Unit API started on port ' + apiPort + '.');
});

// ------------
// WEBSERVER
// ------------

// set template engine
webserver.set('view engine', 'pug');
webserver.set('views', __dirname + "/views");

// use bodyparser
webserver.use(bodyParser.urlencoded({extended: true}));

// serve static files
webserver.use(express.static(__dirname + '/app/assets'));

// require the webserver routes
require('./app/routes/web_routes')(webserver, {});

// listen to our port
webserver.listen(webPort, () => {
   Logger.log('info', 'Swarm Unit webserver started.');
});