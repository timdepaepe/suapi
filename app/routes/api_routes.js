/**
 * The API routes
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

// import modules
var check                   = require('check');
const config                = require('config');
const shell                 = require('shelljs');
const Unit                  = require('../model/unit');

// import models
let Logger                  = require(global.__modeldir + '/logger.js');

module.exports = function(app, db)
{
    // ------------------
    // GET METHODS
    // ------------------

    // ------------------
    // Send out a Hello World
    // @author Tim De Paepe <tim.depaepe@gmail.com>
    // ------------------
    app.get('/helloworld', (req, res) =>
    {
        try
        {
            // result
            res.status(200).send("Hello World.");
        }
        catch(e)
        {
            // error message
            var errorMessage = "helloworld Failed: " + e.message;

            // log the error
            Logger.log('error', errorMessage);

            // send error result
            res.status(500).send("ERROR: " + errorMessage);
        }
    });

    // ------------------
    // Update PD
    // @author Tim De Paepe <tim.depaepe@gmail.com>
    // ------------------
    app.get('/updatepd', (req, res) =>
    {
        try
        {
            // updating the API
            Logger.log('info', "Pulling the latest code from the Pure Data repo.");

            // result
            res.status(200).send("Started pulling the latest code from the Pure Data repo.");

            // execute the shell script to restart the api
            shell.exec(global.__basedir + '/scripts/update-supd.sh');
        }
        catch(e)
        {
            // error message
            var errorMessage = "updatepd Failed: " + e.message;

            // log the error
            Logger.log('error', errorMessage);

            // send error result
            res.status(500).send("ERROR: " + errorMessage);
        }
    });

    // ------------------
    // Update startup scripts
    // @author Tim De Paepe <tim.depaepe@gmail.com>
    // ------------------
    app.get('/updatestartup', (req, res) =>
    {
        try
        {
            // updating the API
            Logger.log('info', "Pulling the latest code from the Startup Scripts repo.");

            // result
            res.status(200).send("Started pulling the latest code from the Startup Scripts repo.");

            // execute the shell script to restart the api
            shell.exec(global.__basedir + '/scripts/update-sustartup.sh');
        }
        catch(e)
        {
            // error message
            var errorMessage = "updatestartup Failed: " + e.message;

            // log the error
            Logger.log('error', errorMessage);

            // send error result
            res.status(500).send("ERROR: " + errorMessage);
        }
    });

    // ------------------
    // Update the API
    // @author Tim De Paepe <tim.depaepe@gmail.com>
    // ------------------
    app.get('/updateapi', (req, res) =>
    {
        try
        {
            // updating the API
            Logger.log('info', "Pulling the latest code from the API repo.");

            // result
            res.status(200).send("Started pulling the latest code from the API repo.");

            // execute the shell script to restart the api
            shell.exec(global.__basedir + '/scripts/update-suapi.sh');
        }
        catch(e)
        {
            // error message
            var errorMessage = "updateapi Failed: " + e.message;

            // log the error
            Logger.log('error', errorMessage);

            // send error result
            res.status(500).send("ERROR: " + errorMessage);
        }
    });

    // ------------------
    // Gets data from our unit
    // @author Tim De Paepe <tim.depaepe@gmail.com>
    // ------------------
    app.get('/info', (req, res) =>
    {
        try
        {
            // updating the API
            Logger.log('info', "Getting the info from our swarm unit");

            // get some data
            let pdVersion = Unit.getPdVersion();
            let apiVersion = Unit.getApiVersion();
            let startupScriptsVersion = Unit.getStartupScriptsVersion();
            let unitNumber = Unit.getUnitNumber();
            let ipAddress = Unit.getIpAddress();
            let audioFiles = Unit.getAudioFiles();

            // get some data
            var data = {
                apiVersion,
                pdVersion,
                startupScriptsVersion,
                unitNumber,
                ipAddress,
                audioFiles
            };

            // answer with our data
            res.json(data);

        }
        catch(e)
        {
            // error message
            var errorMessage = "updateapi Failed: " + e.message;

            // log the error
            Logger.log('error', errorMessage);

            // send error result
            res.status(500).send("ERROR: " + errorMessage);
        }
    });

    // ------------------
    // POST METHODS
    // ------------------

    // ------------------
    // Remove an audio file
    // @author Tim De Paepe <tim.depaepe@letssnap.be>
    // ------------------
    app.post('/deleteAudioFile', (req, res) =>
    {
        res.setHeader('Content-Type', 'application/json');

        // get the body data
        var bodyParams = req.body;

        try
        {
            // validate incoming variables
            check(bodyParams)
                .has('audioFile')
                .assert();

            // log
            Logger.log('info', "Deleting Audio file (" + bodyParams.audioFile + ")");

            // remove audio file
            Unit.deleteAudioFile(bodyParams.audioFile);

            // result
            res.status(200).send("Removed audio file (" + bodyParams.audioFile + ")");
        }

        catch(e)
        {
            // error message
            var errorMessage = "deleteAudioFile Failed: " + e.message;

            // log the error
            Logger.log('error', errorMessage);

            // send error result
            res.status(500).send("ERROR: " + errorMessage);
        }
    });
};