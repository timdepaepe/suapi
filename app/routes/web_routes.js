/**
 * The API routes
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

const config        = require('config');
const fs            = require('fs');
const Unit          = require('../model/unit');

module.exports = function(app, db)
{
    // ------------------
    // GET METHODS
    // ------------------

    // ------------------
    // Get the default web page
    // @author Tim De Paepe <tim.depaepe@gmail.com>
    // ------------------
    app.get('/', (req, res) =>
    {
        let pdVersion = Unit.getPdVersion();
        let apiVersion = Unit.getApiVersion();
        let startupScriptsVersion = Unit.getStartupScriptsVersion();

        // render the index file
        res.render('index', { author: config.get('author'), apiVersion: apiVersion, pdVersion: pdVersion, startupVersion: startupScriptsVersion });
    });
};