/**
 * The Unit class to help us get some stuff
 *
 * @author Tim De Paepe<tim.depaepe@gmail.com>
 */

const config        = require('config');
const fs            = require('fs');

class Unit
{
    static getApiVersion() {
        return config.get('version');
    }

    static deleteAudioFile(audioFile) {
        fs.unlinkSync(`${config.get('audiopath')}/${audioFile}`);
    }

    static getPdVersion() {
        // get the pdpath
        var pdpath = config.get('pdpath');
        var pdinfoPath = pdpath + "/info.json";

        // read the pd json file
        let pdVersion = "Unknown";
        if(fs.existsSync(pdinfoPath)) {
            let pdjsonData = fs.readFileSync(pdinfoPath);
            let pdjson = JSON.parse(pdjsonData);
            pdVersion = pdjson.version;
        }

        // return th pd version
        return pdVersion;
    }

    static getUnitNumber() {
        // get the number path
        var numberPath = config.get('numberpath');

        // read the pd json file
        let number = 0;
        if(fs.existsSync(numberPath)) {
            number = parseInt(fs.readFileSync(numberPath));
        }

        // return the number
        return number;
    }

    static getIpAddress() {
        // get the unit number
        var unitNumber = this.getUnitNumber();

        // create the IP address
        var ipStart = config.get('ipStart');

        // return the ip address
        return `${ipStart}${unitNumber + 10}`
    }

    static getAudioFiles() {
        // get the audio path
        var audioPath = config.get('audiopath');

        // the audio files
        var audioFiles = [];

        // read the pd json file
        if(fs.existsSync(audioPath))
        {
            // the files
            var audioFiles = fs.readdirSync(audioPath);

            // filter out the necessary ones
            audioFiles = audioFiles.filter((file) => {
                var splittedFileName = file.split('.');
                return splittedFileName[1] == "wav";
            })

            // map with full path
            audioFiles = audioFiles.map((file) => {
                var splittedFileName = file.split('.');
                return {
                    file,
                    filePath: `${audioPath}/${file}`,
                    fileName: splittedFileName[0],
                    extension: splittedFileName[1]
                }
            });
        }

        // return the number
        return audioFiles;
    }

    static getStartupScriptsVersion() {
         // get the statuppath
         var startuppath = config.get('startuppath');
         var startupinfoPath = startuppath + "/info.json";
         let startupVersion = "Unknown";

         // read the startup scripts version
         if(fs.existsSync(startupinfoPath)) {
             let startupjsonData = fs.readFileSync(startupinfoPath);
             let startupjson = JSON.parse(startupjsonData);
             startupVersion = startupjson.version;
         }

         // return the startup version
         return startupVersion;
    }
}

module.exports = Unit;