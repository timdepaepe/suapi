/**
 * The Logger
 *
 * @author Tim De Paepe <tim.depaepe@letssnap.be>
 */

'use strict';

 // -------------------
 // IMPORTS
 // -------------------

// requires
const winston       = require('winston');
var config          = require('config');

var initialized     = false;

 // -------------------
 // DEFINITIONS
 // -------------------

class Logger
{
    static log(type, message)
    {
        // init if needed
        if(!initialized) Logger.initialize();

        // log via winston
        winston.log(type, message);
    }

    static initialize()
    {
        winston.configure({
            transports: [
                new (winston.transports.Console)(),
                new (winston.transports.File)({ filename: config.get('logFile') })
            ]
        });

        initialized = true;
    }
}

module.exports = Logger;